/**
 * config
 */

/* Node modules */

/* Third-party modules */

/* Files */
const leaderboard = require('./plugins/leaderboard');

module.exports = {
  title: 'The Bee Frame Challenge',
  description: 'The bee frame making world championship of the world',
  additionalPages: (...args) => leaderboard(...args),
  themeConfig: {
    nav: [{
      text: 'Leader Board',
      link: '/leaderboard/',
    }, {
      text: 'The Rules',
      link: '/rules/',
    }, {
      text: 'Source Code',
      link: 'https://gitlab.com/mrsimonemms/beeframechallenge',
    }],
  },
};
