/**
 * leaderboard
 */

/* Node Modules */
const fs = require('fs');
const path = require('path');

/* Third-Party Modules */
const moment = require('moment');

/* Files */
const leaderboard = require('../../../leaderboard');

function asTime(number) {
  if (number < 10) {
    return `0${number}`;
  }

  return `${number}`;
}

function getLeaderboard(maxPosition = null) {
  let content = `| Position | Name | Time |
| -------- | ---- | ---- |`;

  leaderboard
    .map((item) => {
      /* Limit each time to 1 DP */
      const time = (Math.round(item.time / 100) * 100);

      return {
        ...item,
        time,
      };
    })
    .sort(({ time: a }, { time: b}) => {
      if (a < b) {
        return -1;
      }
      if (a > b) {
        return 1;
      }

      return 0;
    })
    .filter((item, key) => {
      if (maxPosition === null) {
        return true;
      }

      return key < maxPosition;
    })
    .map((item, key, arr) => {
      const previousKey = key - 1;
      item.isEqual = false;
      if (previousKey >= 0) {
        /* There is a previous */
        const previous = arr[previousKey];

        if (item.time === previous.time) {
          /* We have a draw */
          item.position = previous.position;
          item.isEqual = true;
          previous.isEqual = true;
        } else {
          /* Increment the position */
          item.position = key + 1;
        }
      } else {
        /* Pole position */
        item.position = 1;
      }

      return item;
    })
    .forEach((item) => {
      const duration = moment.duration(item.time);

      const position = item.position;
      const isEqual = item.isEqual ? '=' : '';
      let name = item.name;
      if (item.twitter) {
        name = `[${name}](https://twitter.com/${item.twitter})`;
      }
      const time = {
        min: duration.get('minutes'),
        sec: asTime(duration.get('seconds')),
        ms: duration.get('milliseconds') / 100,
      };

      let timeStr = `${time.min}:${time.sec}`;
      if (time.ms > 0) {
        timeStr += `.${time.ms}`;
      }

      if (item.video) {
        timeStr = `[${timeStr}](${item.video})`;
      }

      content += `
| ${position}${isEqual} | ${name} | ${timeStr} |`
    });

  return content;
}

module.exports = () => {
  const top10 = getLeaderboard(10);

  const index = fs.readFileSync(path.join(__dirname, '..', '..', 'index.md'), 'utf8')
    .replace('%TOP_TEN%', top10);

  return [{
    content: index,
    path: '/',
  }, {
    content: `# Leader Board

${getLeaderboard()}`,
    path: '/leaderboard/',
  }]
};
