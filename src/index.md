# The Bee Frame Challenge

Can you make a bee frame in the quickest time possible?

## Rules

This is to find out who can build a frame in the fastest time possible.
You can use any standard frame with wax foundation and it must be in
a usable condition.

Check out the full [rules](rules) before entering.

## Submit your attempt

Enter by tweeting to the [#BeeFrameChallenge](https://twitter.com/intent/tweet?hashtags=BeeFrameChallenge&ref_src=beeframechallenge.com%5Etfw&text=I've%20done%20the%20Bee%20Frame%20Challenge.%20I%20challenge%20<insert>%20to%20beat%20me%0AMy%20time:%0AMy%20video:%20<optional>%0A)

Videos are not required, but recommended. Especially if you get a
really good time.

## Top 10

%TOP_TEN% 
