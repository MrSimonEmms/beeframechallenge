# Rules

The rules of the Bee Frame Challenge

1. Use a standard Hoffman frame with wax foundation. All variations
will count, but a super is advised as will be slightly quicker to get
the foundation in.
2. You may prepare your working area, components, tools, fixings and 
foundation before you start.
3. You may *NOT* split the top bar to remove the wooden splint before
you start.
4. Time starts when you split the top bar and ends when the frame is
complete.
5. The frame must be fully assembled and suitable to use in a hive.
Any frame with missing nails, parts or that would require adjustment
before use is disqualified.
6. You may only use a hammer, knife and hive tool. Any electrical tool
or powered in any way is not allowed.
7. You can have as many goes as you want.
8. This is a friendly bit of competition. Smack talk is allowed in
the spirit of friendly competition, but don't be horrible. Anyone
being horrible or not in the spirit of fun will be disqualified.

If you've never built one before, watch this video on how to build
one

<iframe width="560" height="315" src="https://www.youtube.com/embed/1V5ryOrUyys" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
